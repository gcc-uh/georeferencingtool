
<!-- README.md is generated from README.Rmd. Please edit that file -->

# georeferencingTool

<!-- badges: start -->
<!-- badges: end -->

This package contains R functions intended to facilitate the
georeferencing of collections of text strings describing
localities/study areas.

## Installation

You can install the package from GitLab.

``` r
devtools::install_git("https://gitlab.com/gcc-uh/georeferencingtool")
```

## Example

A simple example:

``` r
library(georeferencingTool)

my.data <- data.frame(
  Address = c("Viikinkaari 1, Helsinki", "Nuuksio National Park, Nuuksiontie, Espoo"),
  Country = c("Finland", "Finland"),
  Lat = 0,
  Long = 0
)

# inspect my.data
my.data
#>                                     Address Country Lat Long
#> 1                   Viikinkaari 1, Helsinki Finland   0    0
#> 2 Nuuksio National Park, Nuuksiontie, Espoo Finland   0    0

#focusing on line 1 of my.data (Viikinkaari)
line <- 1
geo_res <- get_georesult(address = my.data[line, "Address"], country = my.data[line, "Country"])
#> Passing 1 address to the ArcGIS single address geocoder
#> Query completed in: 1.7 seconds
#> Passing 1 address to the Nominatim single address geocoder
#> Query completed in: 2.7 seconds

# this are the found coordinates, after contacting ArcGIS and
# Nominatim/OpenStreetMap servers [using function geo() from tidygeocoder library]
geo_res$data
#>                            address      lat     long method
#> 1 Viikinkaari 1, Helsinki, Finland 60.22558 25.01736 arcgis
#> 2 Viikinkaari 1, Helsinki, Finland 60.22557 25.01706    osm

# using interactive = FALSE and PATH.temp = NULL, just for demonstration
# purposes (as it always uses the first row in geo_res, if present)
# to use interactive = TRUE (recommended), you must first create a temporary
# folder to use as PATH.temp!
my.data[line, c("Lat","Long")] <- georef_my_data(georesult = geo_res, PATH.temp = NULL, detail = "local", interactive = FALSE)

#the first line of my.data should now be georeferenced (i.e. should present latitude and longitude values)
my.data
#>                                     Address Country      Lat     Long
#> 1                   Viikinkaari 1, Helsinki Finland 60.22558 25.01736
#> 2 Nuuksio National Park, Nuuksiontie, Espoo Finland  0.00000  0.00000

#focusing on line 2 of my.data (Nuuksio)
line <- 2
geo_res <- get_georesult(address = my.data[line, "Address"], country = my.data[line, "Country"])
#> Passing 1 address to the ArcGIS single address geocoder
#> Query completed in: 0.2 seconds
#> Passing 1 address to the Nominatim single address geocoder
#> Query completed in: 1 seconds

#again, the found coordinates
geo_res$data
#>                                              address      lat     long method
#> 1 Nuuksio National Park, Nuuksiontie, Espoo, Finland 60.28042 24.58481 arcgis
#> 2 Nuuksio National Park, Nuuksiontie, Espoo, Finland 60.29375 24.55798 arcgis
#> 3 Nuuksio National Park, Nuuksiontie, Espoo, Finland 60.20678 24.65578 arcgis
#> 4 Nuuksio National Park, Nuuksiontie, Espoo, Finland 60.29368 24.55761 arcgis
#> 5 Nuuksio National Park, Nuuksiontie, Espoo, Finland       NA       NA    osm

# using interactive = FALSE and PATH.temp = NULL, just for demonstration
# purposes (as it always uses the first row in geo_res, if present)
# to use interactive = TRUE (recommended), you must first create a temporary
# folder to use as PATH.temp!
my.data[line, c("Lat", "Long")] <- georef_my_data(georesult = geo_res, PATH.temp = NULL, detail = "local", interactive = FALSE)

#the second line of my.data should now be georeferenced too
my.data
#>                                     Address Country      Lat     Long
#> 1                   Viikinkaari 1, Helsinki Finland 60.22558 25.01736
#> 2 Nuuksio National Park, Nuuksiontie, Espoo Finland 60.28042 24.58481
```
